<?php
require (Yii::getAlias('@vendor') . "/autoload.php");
$openapi = \OpenApi\Generator::scan([$path = Yii::getAlias('@app') . "/modules/api/"]);
header('Content-Type: application/json');
echo $openapi->toJson();