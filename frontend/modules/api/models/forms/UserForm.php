<?php

namespace frontend\modules\api\models\forms;

use yii\base\Model;

class UserForm extends Model
{
    public $password;
    public $status;
    public $username;
    public $email;

    public function rules()
    {
        return [
            [['password', 'status', 'username','email'], 'required'],
            [['status'], 'integer'],
            ['email', 'email'],
            [['password', 'username'], 'string', 'max' => 255],
            [['password'], 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Password',
            'status' => 'Status',
            'username' => 'Username',
            'email' => 'E-mail',
        ];
    }
}