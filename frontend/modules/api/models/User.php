<?php

namespace frontend\modules\api\models;

class User extends \common\models\User
{
    /**
     * @OA\Schema(
     *     schema="User",
     *     title="User",
     *     required={"id", "username", "email", "status"},
     *     @OA\Property(
     *         property="id",
     *         type="integer",
     *         format="int64",
     *         description="User ID"
     *     ),
     *     @OA\Property(
     *         property="username",
     *         type="string",
     *         description="Username"
     *     ),
     *     @OA\Property(
     *         property="email",
     *         type="string",
     *         format="email",
     *         description="Email address"
     *     ),
     *     @OA\Property(
     *         property="status",
     *         type="string",
     *         description="User status"
     *     ),
     *     @OA\Property(
     *         property="created_at",
     *         type="string",
     *         format="date-time",
     *         description="Date and time when the user was created"
     *     ),
     *     @OA\Property(
     *          property="password",
     *          type="string",
     *          description="User password"
     *      )
     * )
     */
    public function fields()
    {
        return [
            'id',
            'username',
            'email',
            'status'=> function ($model) {
                return $model->getStatusName();
            },
            'created_at' => function ($model) {
                return date('d.m.Y H:i:s', $model->created_at);
            }
        ];
    }

    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'status'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password_hash', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
        ];
    }


}