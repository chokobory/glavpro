<?php

namespace frontend\modules\api;

use Yii;
use yii\web\Request;
use yii\web\Response;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        \Yii::$app->user->enableSession = false;

        \Yii::configure(\Yii::$app, [
            'components' => [
                'request' => [
                    'class' => \yii\web\Request::class,
                    'cookieValidationKey' => 'TxQtGXttwdOE9b07L6fRz8W-MBZCAt4a',
                    'parsers' => [
                        'application/json' => 'yii\web\JsonParser',
                    ]
                ],
                'response' => [
                    'class' => \yii\web\Response::class,
                    'format' => \yii\web\Response::FORMAT_JSON
                ]
            ]
        ]);
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            'api/users/<id:\w+>' => 'api/user/view',
            'api/users/' => 'api/user'
        ], false);
    }
}
