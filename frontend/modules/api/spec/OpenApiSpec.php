<?php

namespace frontend\modules\api\spec;

use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     version="1.0",
 *     title="user CRUD API",
 *     description="Specification of User REST API",
 * )
 * @OA\Server(
 *     url="http://glavpro.test/api/users",
 *     description="API server"
 * )
 */
class OpenApiSpec
{

}