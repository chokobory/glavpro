<?php

namespace frontend\modules\api\controllers;

use frontend\modules\api\models\forms\UserForm;
use frontend\modules\api\models\forms\UserUpdateForm;
use frontend\modules\api\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use OpenApi\Annotations as OA;

/**
 * @OA\Tag(
 *     name="UserController",
 *     description="User related operations"
 * )
 */
class UserController extends Controller
{
    public function beforeAction($action)
    {
        Yii::$app->request->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                ],
            ]
        );
    }

    /**
     * @OA\Get(
     *      path="/api/users",
     *      summary="Get a list of all users",
     *      tags={"user"},
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/User")
     *          )
     *      )
     *  )
     */
    public function actionIndex()
    {
        if ($this->request->isPost)
            return $this->actionCreate();

        return User::find()->all();
    }

    /**
     * @OA\Get(
     *      path="/api/users/{id}",
     *      summary="Get user by ID",
     *      tags={"user"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="ID of the user",
     *          @OA\Schema(
     *              type="integer",
     *              format="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="User not found"
     *      ),
     *     @OA\Response(
     *           response=404,
     *           description="Invalid input"
     *       )
     *  )
     * @throws BadRequestHttpException
     */
    public function actionView($id)
    {
        if ($this->request->isPut)
            return $this->actionUpdate($id);

        if ($this->request->isDelete)
            return $this->actionDelete($id);

        $user = User::find()->where(['id' => $id])->one();

        if ($user)
            return $user;
        else
            throw new BadRequestHttpException('User not found');
    }

    /**
     * @OA\Post(
     *      path="/api/users",
     *      summary="Create a new user",
     *      tags={"user"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="User created successfully",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *           response=422,
     *           description="Invalid input"
     *       )
     *  )
     */
    public function actionCreate()
    {
        $model = new UserForm();
        $model->load(Yii::$app->request->post(), '');

        if ($model->validate()) {
            $user = new User();
            $user->setPassword($model->password);
            $user->generateAuthKey();
            $user->username = $model->username;
            $user->status = $model->status;
            $user->email = $model->email;

            if ($user->save())
                return $user;
            else {
                Yii::$app->response->statusCode = 422;
                return $user->errors;
            }

        } else {
            Yii::$app->response->statusCode = 422;
            return $model->errors;
        }
    }

    /**
     * @OA\Put(
     *      path="/api/users/{id}",
     *      summary="Update user by ID",
     *      tags={"user"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="ID of the user",
     *          @OA\Schema(
     *              type="integer",
     *              format="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User updated successfully",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Invalid input"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="User not found"
     *      )
     * )
     */

    public function actionUpdate($id)
    {
        $user = User::findOne($id);

        if ($user === null) {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'User not found'];
        }

        $model = new UserUpdateForm();
        $model->load(Yii::$app->request->post(), '');

        if ($model->validate()) {
            $attributes = array_filter($model->getAttributes(), function ($value) {
                return $value !== null;
            });
            $user->setAttributes($attributes);

            if ($user->save()) {
                return $user;
            } else {
                Yii::$app->response->statusCode = 422;
                return $user->errors;
            }
        } else {
            Yii::$app->response->statusCode = 422;
            return $model->errors;
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/users/{id}",
     *      summary="Delete user by ID",
     *      tags={"user"},
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="ID of the user",
     *          @OA\Schema(
     *              type="integer",
     *              format="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User deleted successfully"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="User not found"
     *      )
     *  )
     */
    public function actionDelete($id)
    {
        $user = User::find()
            ->where(['id' => $id])
            ->andWhere(['!=', 'status', 0])
            ->one();

        if (!$user) {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'User not found'];
        } else {
            $user->status = 0;
            $user->save();

            Yii::$app->response->statusCode = 200;
            return ['message' => 'User deleted successfully'];
        }
    }
}