<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\web\Controller;

class DocumentationController extends Controller
{
    public function actionIndex()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        return $this->render('index');
    }
}