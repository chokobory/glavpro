<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class HelloController extends Controller
{
    public function actionAddUsers()
    {
        $model = new User();
        $model->username = 'admin2';
        $model->email = 'admin@mail.com';
        $model->setPassword('123456');
        $model->generateAuthKey();
        $model->status = User::STATUS_ACTIVE;
        $model->save();
        var_dump($model->errors);
    }
}